import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { BarcodeScanner } from "ionic-native";
import { ScanResultPage } from "../scan-result/scan-result.ts";
import { PizzaService } from '../../providers/pizza.service';

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
  providers: [PizzaService]
})
export class ScanPage {
  public scannedText: string;
  public buttonText: string;
  public loading: boolean;
  private eventId: number;
  public eventTitle: string;

  constructor(
    private _nav: NavController,
    private _navParams: NavParams,
    private pizzaService: PizzaService) {
  }

  ionViewDidLoad() {
    this.eventId = this._navParams.get('eventId');
    this.eventTitle = this._navParams.get('eventTitle');

    this.buttonText = "Scan";
    this.loading = false;
  }

  public scanQR() {
    this.buttonText = "Loading..";
    this.loading = true;

    BarcodeScanner.scan().then((barcodeData) => {
      console.log("Scanned successfully!");
      /*
      if (barcodeData.cancelled) {
        console.log("User cancelled the action!");
        this.buttonText = "Scan";
        this.loading = false;
        return false;
      }*/
      console.log("Scanned successfully!");
      console.log(barcodeData);
      this.pizzaService.approvePizza(barcodeData.text);
      console.log("Scanned successfully!" + barcodeData.text);
      this.goToResult(barcodeData);
    }, (err) => {
      console.log(err);
    });
  }

  private goToResult(barcodeData) {
    console.log("--->" + barcodeData.text);
    this._nav.push(ScanResultPage, {
      scannedText: barcodeData.text
    });
  }

  private checkPass(data) {
  }
}
