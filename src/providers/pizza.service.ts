import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

/*
  Generated class for the PizzaService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export class config {
    public static readonly API_END_POINT = "http://172.20.10.6:8000";
}

@Injectable()
export class PizzaService {
  http: any;

  constructor(http:Http) {
    //console.log('PizzaService Provider');
    this.http = http;
  }
  
  approvePizza(contract){
        console.log(contract);
        let body = JSON.stringify({
            "password": "thepass",
            "method": "rateSatisfaction",
            "args": {
            "isHappy": true
            },
            "contract": "PizzaContract",
            "txParams":{
            "gasLimit" : 100000000,
            "gasPrice" : 1
            }
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        console.log("send http post !");
        console.log(contract);

        return this.http.post(config.API_END_POINT + '/users/oracle/cb6769dbd3314bdd198b204a60329f009ffb76de/contract/Pizza/' + contract + '/call', 
                body,
                options)
        .subscribe(data => {
            console.log(data._body);
        }, error => {
            console.log("Oooops!");
            this.handleError(error);
        });
        /*
        return this.http.post(, body,options)
            .map(res => res.json())
            .catch(this.handleError); */
  }

  handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }    
}
